﻿using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult SearchDetail()
        {
            return View();
        }

        public ActionResult SearchAdvance()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResultSearch()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            return View();
        }

        public ActionResult DetailMore(int id)
        {
            return View();
        }

        // Other page
        public ActionResult About()
        {
            return View();
        }

        public ActionResult Service()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

    }
}
